<?php

class Articles{
    private $_Id_article;
    private $__Categorie_article;
    private $_Type_article;
    private $_Nom_article;
    private $_Descriptif_article;
    private $_Montant_credit;
    private $_Date_publi;
    private $_Date_modif;
    private $_Proprietaire;

    public function __construct(Array $infos)
    {
        // hydrater les objets
        $this->hydrate($infos);  
    }
    private function hydrate(Array $infos){

        foreach ($infos as $key => $value) {
          $methode = "set".ucfirst($key); // permet de savoir quel setter appeler
    
          if(method_exists($this, $methode)){
            $this->$methode($value);
          }
        }
    }

    private function setId_article(int $Id_article){
        $this->_Id_article = $Id_article; 
    }
    public function getId_article(){
        return $this->_Id_article;
    }

    private function setCategorie_article(string $Categorie_article){
        $this->__Categorie_article = htmlspecialchars($Categorie_article);
    }
    public function getCategorie_article(){
        return htmlspecialchars_decode ($this->__Categorie_article);
    }

    private function setType_article(string $Type_article){
        $this->_Type_article = htmlspecialchars($Type_article);    
    }
    public function getType_article(){
        return htmlspecialchars_decode($this->_Type_article);
    }

    private function setNom_article(string $Nom_article){
        $this->_Nom_article = htmlspecialchars($Nom_article) ; 
    }
    public function getNom_article(){
        return htmlspecialchars_decode($this->_Nom_article);
    }
    
    private function setDescriptif_article(string $Descriptif_article){
        $this->_Descriptif_article = htmlspecialchars($Descriptif_article);         
    }
    public function getDescriptif_article(){
        return htmlspecialchars_decode($this->_Descriptif_article);
    }
    
    private function setMontant_credit(string $Montant_credit){
        $this->_Montant_credit = htmlspecialchars($Montant_credit);         
    }
    public function getMontant_credit(){
        return htmlspecialchars_decode($this->_Montant_credit);
    }

    private function setDate_publi(string $Date_publi){
        $this->_Date_publi = $Date_publi;
    }
    public function getDate_publi(){
        return $this->_Date_publi;
    }
    
    private function setDate_modif(string $Date_modif){
        $this->_Date_modif = $Date_modif;
    }
    public function getDate_modif(){
        return $this->_Date_modif;
    }
    
    private function setProprietaire(string $Proprietaire){
        $this->_Proprietaire = htmlspecialchars ($Proprietaire); 
    }
    public function getProprietaire(){
        return htmlspecialchars_decode ($this->_Proprietaire);
    }
    
}