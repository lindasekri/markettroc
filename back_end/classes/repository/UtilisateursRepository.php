<?php
class UtilisateursRepository {
  // Attributs
  private $_db;

  // Constructeur
  public function __construct(){
    $Database = new Database();
    $this->_db = $Database->getBDD();

  }
  
  public function getAllUtilisateurs() {
    $sql = "SELECT Nom, prenom FROM utilisateurs";
    $req = $this->_db->prepare($sql);
    $req->execute();
    
    $resultat = $req->fetchAll(PDO::FETCH_ASSOC); 
    return $resultat;
  }
  
  // seletionner un utilisateur 
  public function getUtilisateur($Id) {
    
    $sql = "SELECT * FROM utilisateurs WHERE Id = :Id"; // Sélectionnez toutes les colonnes
    $requete = $this->_db->prepare($sql);
    $requete->execute([':Id' => $Id]);
    $infos = $requete->fetch(PDO::FETCH_ASSOC); // Utilisez fetch au lieu de fetchAll pour un seul utilisateur
    if ($infos) {
      $utilisateur = new Utilisateurs($infos);
      return $utilisateur;
    } else {
      return null; // Gérer le cas où l'utilisateur n'est pas trouvé
    }
}
// creer
public function creerUtilisateurs($infos) {
  $sql = 'INSERT INTO utilisateurs (Nom, Prenom, Pseudo, Email, Mot_de_passe) VALUES (:Nom, :Prenom, :Pseudo, :Email, :Mot_de_passe)';

  try {
    $requete = $this->_db->prepare($sql);
    $requete->bindValue(':Nom', $infos['Nom']);
    $requete->bindValue(':Prenom', $infos['Prenom']);
    $requete->bindValue(':Pseudo', $infos['Pseudo']);
    $requete->bindValue(':Email', $infos['Email']);
    $requete->bindValue(':Mot_de_passe', $infos['Mot_de_passe']);
    
    $requete->execute();
 echo"je suis $requete"; 
    return true;
  } catch (PDOException $e) {
    echo "Erreur d'enregistrement de l'utilisateur : " . $e->getMessage();
    return false;
  }
  
}
  


// modifier
  public function modifierUtilisateurs(Array $infos){
    $retour1 = $this->supprimerUtilisateurs($infos['Id']);
    $retour2 = $this->creerUtilisateurs($infos);

    if($retour1 === TRUE && $retour2 === TRUE){
      return TRUE;
    }
  }

//supprimer
  public function supprimerUtilisateurs($Id){
    $sql = "DELETE FROM utilisateurs WHERE Id = :Id";
    try{
      $requete = $this->_db->prepare($sql);
      $requete->execute([':Id'=>$Id]);

      return TRUE;
    } catch (PDOException $e){
      echo "erreur de suppression de l'article : " .$e->getMessage();
    }
  }


  public function chercheUtilisateurs($Email){
    $sql = "SELECT * FROM utilisateurs WHERE Email = :Email";
    try{
      $requete = $this->_db->prepare($sql);
      $requete->execute([':Email'=>$Email]);

      return TRUE;
    } catch (PDOException $e){
      echo "erreur de suppression de l'article : " .$e->getMessage();
    }
  }
  
}


