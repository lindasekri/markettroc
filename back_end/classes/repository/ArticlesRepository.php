<?php
class AriclesRepository {
  // Attributs
  private $_db;

  // Constructeur
  public function __construct(){
    $Database = new Database();
    $this->_db = $Database->getBDD();
  }

  public function getArticles(){
    $sql = "SELECT * FROM articles";

    $req = $this->_db->prepare($sql);
    $req->execute();

    $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    foreach ($resultat as $key => $infos) {
      $articles[$key] = new Articles($infos);
    }
    return $articles;
  }

// seletionner un Articles 

  public function getOneArticle($Id){
    $sql = "SELECT Id FROM articles Where Id = Id";
    $requete = $this->_db->prepare($sql);
    $requete->execute([':Id'=>$Id]);
    $infos = $requete->fetchAll(PDO::FETCH_NUM);

    

  }
// creer
  public function creerArticles($infos){
    $sql = 'INSERT INTO articles (Categorie_article, Type_article, Nom_article,	Descriptif_article, 	Montant_credit,	Date_publi,	Date_modif,	Proprietaire) VALUES (:Categorie_article, :Type_article,	:Nom_article, :Descriptif_article, :Montant_credit, :Date_publi, :Date_modif,	:Proprietaire)';
     try{
      $requete = $this->_db->prepare($sql);
      $requete->execute([':Categorie_article'=>$infos['Categorie_article'],
                         ':Type_article'=>$infos['Type_article'],
                         ':Nom_article'=>$infos['Nom_article'],
                         ':Descriptif_article'=>$infos['Descriptif_article'],
                         ':Montant_credit'=>$infos['Montant_credit'],
                         ':Date_publi'=>$infos['Date_publi'],
                         ':Date_modif'=>$infos['Date_modif'],
                         ':Proprietaire'=>$infos['Proprietaire'] ]);
      return TRUE;
    } catch (PDOException $e){
      echo "erreur d'enregistrement de l'article : " .$e->getMessage();
    }
  }
  
// modifier
  public function modifierArticles(Array $infos){
    $retour1 = $this->supprimerArticles($infos['Id']);
    $retour2 = $this->creerArticles($infos);

    if($retour1 === TRUE && $retour2 === TRUE){
      return TRUE;
    }
  }

//supprimer
  public function supprimerArticles($Id){
    $sql = "DELETE FROM articles WHERE Id = :Id";
    try{
      $requete = $this->_db->prepare($sql);
      $requete->execute([':Id'=>$Id]);

      return TRUE;
    } catch (PDOException $e){
      echo "erreur de suppression du livre : " .$e->getMessage();
    }
  }

}


