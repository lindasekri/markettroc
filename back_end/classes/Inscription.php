<?php
class Inscription {
    private $db;

    public function __construct() {
        // Connect to the database (replace with your database credentials)
        $this->db = new mysqli('localhost', 'markettroc', 'markettroc', 'markettroc');

        // Check connection
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    public function registerUser($data) {
        // Get data from POST request
        $nom = $data['Nom'];
        $prenom = $data['Prenom'];
        $pseudo = $data['Pseudo'];
        $email = $data['Email'];
        $mot_de_passe = $data['Mot_de_passe'];

        // Insert data into database
        $query = "INSERT INTO users (Nom, Prenom, Pseudo, Email, Mot_de_passe) VALUES ('$nom', '$prenom', '$pseudo', '$email', '$mot_de_passe')";
        if ($this->db->query($query) === TRUE) {
            return array("Status" => "success");
        } else {
            return array("Status" => "error");
        }
    }

    public function closeConnection() {
        // Close the database connection
        $this->db->close();
    }
}

// Create an instance of the Inscription class
$inscription = new Inscription();

// Check if the request is a POST request
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get the JSON data from the request body
    $data = json_decode(file_get_contents('php://input'), true);

    // Register the user
    $result = $inscription->registerUser($data);

    // Return the result as JSON response
    header('Content-Type: application/json');
    echo json_encode($result);
}

// Close the database connection
$inscription->closeConnection();

