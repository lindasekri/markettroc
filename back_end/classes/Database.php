<?php

final class Database {

  private $_db;

  //constructeur
  public function __construct(){
    require __DIR__ . "/../config.php";
    $this->connexionDB($CONFIG);
  }
  //methodes

  public function connexionDB($CONFIG){
    try {
      $dsn = "mysql:host=" . $CONFIG['DB_HOST'] . ";dbname=" . $CONFIG['DB_NAME'];
      $this->_db = new PDO($dsn,$CONFIG['DB_USER'],$CONFIG['DB_MDP'],array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      return $dsn;

    }catch(PDOException $erreur){
      echo "Erreur de connexion : " . $erreur->getMessage();
    }
  }

  public function getBDD(){
    return $this->_db;
  }

  public function closeBDD(){
    $this->_db = NULL;
  }
}
