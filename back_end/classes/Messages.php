<?php
class Messages{
    private $_Id;
    private $_Lu;
    private $_Date_recevoir;
    private $_Auteur;
    private $_Destinataire;
    private $_Article;
    public function __construct(Array $infos)
    {
        // hydrater les objets
        $this->hydrate($infos);  
    }
    private function hydrate(Array $infos){

        foreach ($infos as $key => $value) {
          $methode = "set".ucfirst($key); // permet de savoir quel setter appeler
    
          if(method_exists($this, $methode)){
            $this->$methode($value);
          }
        }
    }
    
    private function setId(int $Id){
        $this->_Id = $Id;
    }
    public function getId(){
        return $this->_Id;
    }

    private function setLu(string $Lu){
        $this->_Lu = $Lu;
    }
    public function getLu(){
        return $this->_Lu;
    }
    
    private function setDate_recevoir(string $Date_recevoir){
        $this->_Date_recevoir = $Date_recevoir;    
    }
    public function getDate_recevoir(){
        return $this->_Date_recevoir;
    }

    private function setAuteur(string $Auteur){
        $this->_Auteur = htmlspecialchars($Auteur);
    }
    public function getAuteur(){
        return htmlspecialchars_decode($this->_Auteur);
    }

    private function setDestinataire(string $Destinataire){
        $this->_Destinataire = htmlspecialchars($Destinataire);
    }
    public function getDestinataire(){
        return htmlspecialchars_decode($this->_Destinataire);
    }

    private function setArticle(string $Article){
        $this->_Article = htmlspecialchars($Article);
    } 
    public function getArticle(){
        return htmlspecialchars_decode($this->_Article);
    }
}