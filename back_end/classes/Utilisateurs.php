<?php

/**
 * class Utilisateurs
 */
class Utilisateurs{
    // attributs
    private $_Id;
    private $_Nom;
    private $_Prenom;
    private $_Pseudo;
    private $_Email;
    private $_Mot_de_passe;
    
    
    //Le constructeur 
    public function __construct(Array $infos)
    {
        // hydrater les objets
       
        $this->hydrate($infos);   
        
    }
    private function hydrate(Array $infos){

        foreach ($infos as $key => $value) {
          $methode = "set".ucfirst($key); // permet de savoir quel setter appeler
    
          if(method_exists($this, $methode)){
            $this->$methode($value);
          }
        }
    }
    
    // Les méthodes 
    private function setId(int $Id){
        $this->_Id = $Id;
        
    }
    public function getId(){
        return $this->_Id;
    }

    private function setNom(string $Nom){
            $this->_Nom = htmlspecialchars($Nom);
    }
    public function getNom(){
        return htmlspecialchars_decode($this->_Nom);
    }

    private function setPrenom(string $Prenom){
        $this->_Prenom = htmlspecialchars($Prenom);
    }

    public function getPrenom(){
        return htmlspecialchars_decode($this->_Prenom);
    }

    private function setPseudo(string $Pseudo){
        $this->_Pseudo = htmlspecialchars($Pseudo);
    }
    public function getPseudo(){
        return htmlspecialchars_decode ($this->_Pseudo);
    }

    private function setEmail(string $Email){
        $this->_Email = htmlspecialchars($Email);
    }
    public function getEmail(){
        return htmlspecialchars_decode ($this->_Email);
    }

    private function setMot_de_passe(string $Mot_de_passe){
        $this->_Mot_de_passe = htmlspecialchars($Mot_de_passe);
    }
    public function getMot_de_passe(){
        return htmlspecialchars_decode  ($this->_Mot_de_passe);
    }
}