#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateurs
#------------------------------------------------------------

CREATE TABLE Utilisateurs(
        Id           Int  Auto_increment  NOT NULL ,
        Nom          Varchar (50) NOT NULL ,
        Prenom       Varchar (50) NOT NULL ,
        Pseudo       Varchar (50) NOT NULL ,
        Email        Varchar (100) NOT NULL ,
        Date_inscr   Date NOT NULL ,
        Mot_de_passe Varchar (100) NOT NULL ,
        Gestionnaire Bool NOT NULL ,
        Credit       Int NOT NULL
	,CONSTRAINT Utilisateurs_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Articles
#------------------------------------------------------------

CREATE TABLE Articles(
        Id_article         Int  Auto_increment  NOT NULL ,
        Categorie_article  Varchar (50) NOT NULL ,
        Type_article       Varchar (50) NOT NULL ,
        Nom_article        Varchar (50) NOT NULL ,
        Descriptif_article Mediumtext NOT NULL ,
        Montant_credit     Int NOT NULL ,
        Date_publi         Datetime NOT NULL ,
        Date_modif         Datetime NOT NULL ,
        Proprietaire       Int NOT NULL
	,CONSTRAINT Articles_PK PRIMARY KEY (Id_article)

	,CONSTRAINT Articles_Utilisateurs_FK FOREIGN KEY (Proprietaire) REFERENCES Utilisateurs(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Messages
#------------------------------------------------------------

CREATE TABLE Messages(
        Id            Int  Auto_increment  NOT NULL ,
        Message       Text NOT NULL ,
        Lu            Bool NOT NULL ,
        Date          Datetime NOT NULL ,
        Date_recevoir Datetime NOT NULL ,
        Auteur        Int NOT NULL ,
        Destinataire  Int NOT NULL ,
        Article       Int NOT NULL
	,CONSTRAINT Messages_PK PRIMARY KEY (Id)

	,CONSTRAINT Messages_Utilisateurs_FK FOREIGN KEY (Auteur) REFERENCES Utilisateurs(Id)
	,CONSTRAINT Messages_Utilisateurs0_FK FOREIGN KEY (Destinataire) REFERENCES Utilisateurs(Id)
	,CONSTRAINT Messages_Articles1_FK FOREIGN KEY (Article) REFERENCES Articles(Id_article)
)ENGINE=InnoDB;

