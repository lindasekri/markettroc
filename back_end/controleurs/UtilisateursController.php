<?php

class UtilisateursController
{
  private $repository, $CONFIG;
  
  public function __construct()
  {
    $this->repository = new UtilisateursRepository();

    
    include __DIR__ . "/../config.php";
    $this->CONFIG = $CONFIG;
  }
  
  public function saveUtilisateur($infos)
  {
    
    if ( isset($infos['Nom']) && isset($infos['Prenom']) && isset($infos['Pseudo']) && isset($infos['Email']) && isset($infos['Mot_de_passe'])) {
      
      $infos = [
        "Nom" => $infos['Nom'],
        "Prenom" => $infos['Prenom'],
        "Pseudo" => $infos['Pseudo'],
        "Email" => $infos['Email'],
        "Mot_de_passe" => $infos['Mot_de_passe']
      ];
      $repo_utilisateur = new UtilisateursRepository();
      
      $retourcreation = $repo_utilisateur->creerUtilisateurs($infos);
      var_dump("je suis le retour de creation".$retourcreation);
      if ($retourcreation === TRUE) {
        header('location:' . $this->CONFIG['URL_SITE']);
        exit();
      }
    }
  }
  public function updateUtilisateurs($infos)
  {
    
    if (
      isset($_POST['EnregistrerModifs']) && isset($_POST['Nom']) &&
      isset($_POST['Prenom']) && isset($_POST['Pseudo']) && isset($_POST['Email']) && isset($_POST['Mot_de_passe'])
      ) {
        $infos = [
          "Nom" => $_POST['Nom'],
          "Prenom" => $_POST['Prenom'],
          "Pseudo" => $_POST['Pseudo'],
          "Email" => $_POST['Email'],
          "Mot_de_passe" => $_POST['Mot_de_passe']
        ];
        $repo_utilisateur = new UtilisateursRepository();
        $retourmodification = $repo_utilisateur->modifierUtilisateurs($infos);
        
        if ($retourmodification === TRUE) {
          header('location:' . $this->CONFIG['URL_SITE']);
          exit();
        }
    }
  }

  public function deleteUtilisateurs($Id)
  {
    if (isset($_POST['Supprimer']) && isset($_POST['Id'])) {
      $repo_utilisateur = new UtilisateursRepository();
      $retoursuppression = $repo_utilisateur->supprimerUtilisateurs($_POST['Id']);
      if ($retoursuppression === TRUE) {
        header('location:' . $this->CONFIG['URL_SITE']);
        exit();
      }
    }
  }

  public function chercheUtilisateurs($infos)
  {
    if (isset($_POST['Chercher']) && isset($_POST['Email']) && isset($_POST['Mot_de_passe'])) {
      $infos = [
        "Email" => $_POST['Email'],        "Mot_de_passe" => $_POST['Mot_de_passe']
      ];
      $repo_utilisateur = new UtilisateursRepository();
      $retourrecherche = $repo_utilisateur->chercheUtilisateurs($infos);
      if ($retourrecherche === TRUE) {
        header('location:' . $this->CONFIG['URL_SITE']);
        exit();
      }
    }
  }
  
}
