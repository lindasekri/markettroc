import React from 'react'
import './App.css'
import Entete from './composants/organismes/BarreDeNavigation'
import MenuPrincipal from './composants/organismes/MenuPrincipal'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Accueil from './pages/Accueil'
import Inscription from './pages/Inscription';
import Connexion from './pages/Connexion'
import Services from './pages/Services'
import Echanges from './pages/Echanges'
import Dons from './pages/Dons'
import Braderie from './pages/Braderie'
import Profile from './pages/Profil';
import PiedDePage from './composants/organismes/PiedDePage';
import APropos from './pages/APropos';
import Contact from './pages/Contact';
import MentionsLegales from './pages/MentionsLegales';

function App() {
  return (
  <>
    <Router>
      <Entete />
      <MenuPrincipal />
      <Routes>
        <Route path="/" element={<Accueil />} />
        <Route path="/Inscription" element={<Inscription />} />
        <Route path="/Connexion" element={<Connexion />} />
        <Route path="/Services" element={<Services />} />
        <Route path="/Echanges" element={<Echanges />} />
        <Route path="/Dons" element={<Dons />} />
        <Route path="/Braderie" element={<Braderie />} />
        <Route path="/Profile" element={<Profile />} />
        <Route path="/Contact" element={<Contact />} />
        <Route path="/APropos" element={<APropos />} />
        <Route path="/MentionsLegales" element={<MentionsLegales />} />
      </Routes>
      <PiedDePage />
    </Router>
  </>
  )
}
export default App