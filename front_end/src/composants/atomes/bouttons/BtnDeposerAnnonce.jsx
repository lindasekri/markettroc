import React from 'react';
import '../../../App.css';

const BtnDeposerAnnance = () => {
  return (
    <>
      <button className="btnDepos">
        + Déposer une annonce
      </button>
    </>
  );
};

export default BtnDeposerAnnance;
