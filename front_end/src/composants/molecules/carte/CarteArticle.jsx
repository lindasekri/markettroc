import React from 'react'
import styled from "styled-components";

const Catre = styled.section`
  justify-content: center;
  box-shadow: 0 0 10px rgba(39, 38, 38, 0.5);
  border-radius: 20px;
  paddings:auto;
  margin-bottom: 10px;
  width: 350px;
  @media (max-width: 400px) {
    width: 300px;
  }
`;
const Image = styled.img`
  width: 350px;
  height: 300px;
  border-radius: 20px 20px 0 0;
  position: relative;
  z-index: 1;
  @media (max-width: 400px) {
    width: 300px;
`;
const Button = styled.button`
  background-color: #BC632E;
  width: 100px;
  height: 30px;
  box-shadow: 0 5 20 0 rgba(0, 0, 0, 0.1);
  margin-left: 100px;
  color: #fff;
  font-size: 14px;
`;


function CarteArticles(props) {
  return (
    <Catre key={props.id}  >
      <Image src={props.image} alt="" />
    <div className='containerCarte'>
      <h2>{props.pays}</h2>
      <p><b>Déscription: </b><br />{props.description}</p>
      <p><b>Troc contre: {props.prix}</b> </p>
      <p><b>Catégorie: {props.date}</b></p>
      <div>
        <Button>Visiter</Button>
      </div>
      </div>
    </Catre>
  )
}
export default CarteArticles