import styled from 'styled-components'
import { NavLink as Link } from 'react-router-dom'

export const MenuLink = styled(Link)`
color: white;
background-color: #BC632E;
cursor: pointer;
align-items: center;
text-decoration: none;
font-size: 12px;
font-weight: bold; 
margin-right: 10px;
padding: 5px 10px; 
&.active {
  color: black;
}
z-index: 1000;
bottom: 0;
left: 0;
display: flex;
justify-content: space-between; /* Espacement entre les liens */
`;
export const PiedDePageElement = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height:100px;
  @media screen and (max-width: 768px) {
    display: flex;
    flex-direction: column;
    text-align: center;
    justify-content: center;
    height:auto;
    padding-top:30px;
    align-items: center;
    // position: fixed;
    bottom: 0;
    left: 0;
  }
`
