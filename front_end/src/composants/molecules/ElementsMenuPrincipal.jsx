import styled from 'styled-components'
import { NavLink as Link } from 'react-router-dom'

export const MenuPrincipalLink = styled(Link)`
color: white;
display: flex;
cursor: pointer;
align-items: center;
text-decoration: none;
padding: 0 1.2rem;
height: 10%;
font-size: calc(12px + 1.5vmin);
&.active {
  color: black;
}
flex-direction: row;
&:hover {
  color: white; 
}
`;

export const MenuPrincipalElement = styled.div`
  display: flex;
  align-items: right;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;