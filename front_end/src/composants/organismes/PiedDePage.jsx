import React from 'react'
import { MenuLink, PiedDePageElement } from '../molecules/ElementsPiedDePage'

import '../../App.css'

const PiedDePage = () => {
  return (
    <>
    <PiedDePageElement className='PiedDePageElements'>
          <MenuLink to="/Contact" activestyle>
            CONTACT
          </MenuLink>
          <MenuLink to="/APropos" activestyle>
           A PROPOS
          </MenuLink>
          <MenuLink to="/Mentionslegales" activestyle>
           MENTIONS LEGALES
          </MenuLink> 
    </PiedDePageElement>
    </>
  )
}
export default PiedDePage