import React from "react";
import { MenuPrincipalElement, MenuPrincipalLink } from "../molecules/ElementsMenuPrincipal";
import serviceslogo from "../../img/serviceslogo.png"
import echangeslogo from "../../img/echangeslogo.png"
import donslogo from "../../img/donslogo.png"
import braderielogo from "../../img/braderielogo.png"
import '../../App.css'


const MenuPrincipal = () => {
    return(
        <MenuPrincipalElement className="menuprincipal">
            <MenuPrincipalLink to="/Services" activestyle>
            <img  src={serviceslogo} alt="Logo" className='serviceslogo'/><br/>
            Services
            </MenuPrincipalLink>
            <MenuPrincipalLink to="/Echanges" activestyle>
            <img  src={echangeslogo} alt="Logo" className='echangeslogo'/>
            Echanges
            </MenuPrincipalLink>
            <MenuPrincipalLink to="/Dons" activestyle>
            <img  src={donslogo} alt="Logo" className='donslogo'/>
            Dons
            </MenuPrincipalLink>
            <MenuPrincipalLink to="/Braderie" activestyle>
            <img  src={braderielogo} alt="Logo" className='braderielogo'/>
            Braderie
          </MenuPrincipalLink>
        </MenuPrincipalElement>
    )
}

export default MenuPrincipal;