import axios from "axios";
import { useEffect, useState } from "react";
import CarteArticles from "../molecules/carte/CarteArticle";
import styled from "styled-components";
import '../../App.css'
import * as React from "react";

const CarteArticle = styled.div`
display: grid;
grid-template-columns: repeat(3, 1fr );
grid-template-rows: repeat(auto, 1fr );
margin: 5px 5px 5px 5px;
gap: 5% 5%;
z-index: 100;
justify-content: center;
margin-bottom:100px;
margin-top:100px;
@media (max-width: 768px) and (min-width: 375px){
  grid-template-columns: 1fr;
  gap: 50px;
  justify-content: center;
}
@media (max-width: 374px) {
  grid-template-columns: 1fr;
  gap: 50px;
  justify-content: center;
}

@media (min-width: 769px) and (max-width: 1034px) {
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(auto, 1fr);
  gap: 50px;
  justify-content: center;
}
`;

export async function fetchAxios() {
  await axios
    .get("Articles.json")
    .then(function (response) {
      const { data } = response;
      console.log(data.articles);
    })
    .catch(function (error) {
      console.log(error);
    })
    .finally(function () {
    });

}
function DerniersPostes() {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    fetch("Articles.json")
      .then((response) => response.json())
      .then((data) => setArticles(data.articles))
      .catch((error) => console.error(error));
  }, []);
  return (
    <>
      <CarteArticle className="carteArticle">
        {articles.map((article) => (
          <CarteArticles pays={article.pays} prix={article.prix} date={article.date} description={article.description} image={article.image} key={article.id} />
        ))}
      </CarteArticle>
    </>
  )
}
export default DerniersPostes
