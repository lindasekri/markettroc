import React from "react";
import "../../App.css";
import Mon_logo from '../../img/Mon_logo.png'
import BtnDeposerAnnance from '../atomes/bouttons/BtnDeposerAnnonce'
import { useState } from "react";


const Entete = () =>{
  const [showLinks, setShowLinks] =useState(false);
  const handelShowLinks = () => {
    setShowLinks(!showLinks);
    console.log(showLinks);
  }
    return(
    <div className={`navbar ${showLinks ? "show-nav" :"hide-nav"} `}>
      <img src={Mon_logo} alt="Logo" className='Mon_logo' />
      <BtnDeposerAnnance />
      <ul className="navbar_links">
        <li className="navbar_item">
          <a href="/" className="navbar_link">Accueil</a>
        </li>
        <li className="navbar_item">
          <a href="/Inscription" className="navbar_link">Inscription</a>
        </li>
        <li className="navbar_item">
          <a href="/Connexion" className="navbar_link">Connexion</a>
        </li>
        <li className="navbar_item">
          <a href="/Services" className="navbar_link">Services</a>
        </li>
        <li className="navbar_item">
          <a href="/Dons" className="navbar_link">Dons</a>
        </li>
        <li className="navbar_item">
          <a href="/Echanges" className="navbar_link">Echanges</a>
        </li>
        <li className="navbar_item">
          <a href="/Braderie" className="navbar_link">Braderie</a>
        </li>
      </ul>
      <button className="navbar_burger" onClick={handelShowLinks}> 
        <span className="Burger-Bar"></span>
      </button>
    </div>
      )
}
export default Entete