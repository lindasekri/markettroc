
import React from 'react'
import { useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom';
import "../App.css";
import styled from 'styled-components';

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #BC632E;
  margin-top:100px;
  text-align: left;
  margin-left:250px;
  `;
const Title2 = styled.h2`
  font-size: 1rem;
  color: #BC632E;
  text-align: left;
  margin-left:170px;
  margin-top:-10px;
  font-size: 16px;
  `;

function Inscription() {
  const navigate = useNavigate();
  const [data, setData] = useState({
    // Form: "",
    Nom: "",
    Prenom: "",
    Pseudo: "",
    Email: "",
    Mot_de_passe: "",
  })
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });

  }
  const submitForm = (e) => {
    e.preventDefault();
    // console.log("submitForm");
    const sendData = {
      // Form: "inscription",
      Nom: data.Nom,
      Prenom: data.Prenom,
      Pseudo: data.Pseudo,
      Email: data.Email,
      Mot_de_passe: data.Mot_de_passe
    }
    // console.log(sendData);

    axios.post('http://markettroc/back_end/inscription', sendData)
    .then((response) => {
        if (response.data.Status === 'invalid') {
          alert('Utilisateur invalide');
        } else {
          // Vous pouvez également ajouter une redirection ici après succès.
          navigate('/Inscription'); // Utilisation de la fonction navigate pour rediriger
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className='contenairePrincipale'>
      <Title>
        Bienveue 
      </Title>
      <div className='contenaireForm'>
        <Title2>Inscription</Title2>
        <form onSubmit={submitForm}>
          <label>
            
            <input type="text" name="Nom" onChange={handleChange} value={data.Nom}  className='inputtaille' placeholder='Nom'/>
          </label><br />
          <label>
          
            <input type="text" name="Prenom" onChange={handleChange} value={data.Prenom} className='inputtaille'placeholder='Prénom'/>
          </label><br />
          <label>
            <input type="text" name="Pseudo" onChange={handleChange} value={data.Pseudo} className='inputtaille' placeholder='Pseudo'/>
          </label><br />
          <label>
            
            <input type="text" name="Email" onChange={handleChange} value={data.Email} className='inputtaille'placeholder='Email'/>
          </label><br />
          <label>
            <input type="text" name="Mot_de_passe" onChange={handleChange} value={data.Mot_de_passe} className='inputtaille'placeholder=' Mot de passe '/>
          </label><br />
          <button type="submit" value="Entrer" className='btntaille'>Entrer</button>
        </form>
      </div>
    </div>
  )
}
export default Inscription;