import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #BC632E;
  margin-top:100px;
  text-align: left;
  margin-left:250px;
  `;
const Title2 = styled.h2`
  font-size: 1em;
  color: #BC632E;
  text-align: left;
  margin-left:170px;
  margin-top:50px;
  `;

function Connexion() {
  const navigate = useNavigate();
  const [data, setData] = useState({
    FORM: "",
    Email: "",
    Mot_de_passe: "",
  });

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const submitForm = (e) => {
    e.preventDefault();
    const sendData = {
      Form: "connexion",
      Email: data.Email,
      Mot_de_passe: data.Mot_de_passe,
    };

    axios.post('http://markettroc/back_end/connexion', sendData)
      .then((response) => {
        if (response.data.status === 'invalid') {
          alert('Identifiants invalides');
        } else {
          navigate('/Profile'); // Redirigez vers le tableau de bord après une connexion réussie
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      <Title>
        Bienvenue
      </Title>
      <div className='contenaireForm'>
        <Title2>Connexion</Title2>
        <form onSubmit={submitForm}>
          <label>
            <input type="text" name="Email" onChange={handleChange} value={data.Email} className='inputtaille' placeholder='Email' />
          </label><br />
          <label>
            <input type="text" name="Mot_de_passe" onChange={handleChange} value={data.Mot_de_passe} className='inputtaille' placeholder='Mot de passe' />
          </label><br />
          <button type="submit" value="Connexion" className='btntaille'>Entrer</button>
        </form>
      </div>
    </>
  )
}
export default Connexion


