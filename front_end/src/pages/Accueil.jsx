import React from 'react'
import styled from "styled-components";
import DerniersPostes from "../composants/organismes/DerniersPostes";
import "../App.css";


const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #BC632E;
`;
const Form = styled.section`
display: flex;
padding: 1em;
background: fffff;
text-align: center;
justify-content: center;
`;
const SearchInput = styled.input`
border-radius: 14px 14px 14px 14px;
padding: 10px;
border: none;
width: 40%;
box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
`;

const SearchButton = styled.button`
background-color: #BC632E;
color: #ffffff;
border-radius: 14px 14px 14px 14px;
padding: 10px 20px;
border: none;
cursor: pointer;
box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
margin-left: -81px; 
`;
const Wrapper = styled.section`
  margin:auto;
  padding:auto;
  flex-direction: column;
  align-items: center; 
  justify-content: center; 
  background: fffff;
`;

function Accueil() {
  return (
  <>
      <Title>
        Faites votre recherche
      </Title>
      <Form>
        <SearchInput
          type="text"
          placeholder="Recherche"
          name="loc"
        />
        <SearchButton>Search</SearchButton>
      </Form>
    <div className='contenaire'>
      <Wrapper>
        <DerniersPostes />
      </Wrapper>
      
    </div>
    </>
  )
}
export default Accueil